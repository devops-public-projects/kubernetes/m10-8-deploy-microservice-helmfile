
# Deploy a Microservice with Helmfile

## Technologies Used
- Kubernetes
- Helm
- Helmfile

## Project Description
- Deploy Microservices with Helm
- Deploy Microservices with Helmfile

## Prerequisites
- [Previous Project Completed](https://gitlab.com/devops-public-projects/kubernetes/m10-7-microservice-helm-chart)

## Guide Steps
These steps are for the pre-Helmfile conversion.

### Create an Install and Uninstall Shell Script
For each Microservice, we need to install or uninstall it. In order to consolidate that into two respective files, we will create an `install.sh` and an `uninstall.sh`. Inside each will contain a `helm install` or `helm uninstall` command.

```yaml
#Example Install File
helm install -f values/redis-values.yaml rediscart charts/redis

helm install -f values/email-service-values.yaml emailservice charts/microservice
#Other Microservices listed here
```

```yaml
#Example Uninstall File
helm uninstall rediscart
helm uninstall emailservice
#Other Microservices listed here
```

### Run Script
- Start Minikube
	- `minikube start`
- Modify Script Permissions
	- `chmod u+x install.sh`
	- `chmod u+x uninstall.sh`
- `./install.sh`

![Install Script Success](/images/m10-8-install-success.png)

![Pods are Running](/images/m10-8-pods-running.png)

---

### Helmfile Creation

- We will create a `helmfile.yaml` in the project directory and fill it with our 11 microservices. This information is what we had in our `config.yaml` and placed into the `install.sh` from before.
```yaml
#Example helmfile.yaml
releases:
 - name: redis-cart
   chart: charts/redis
   values:
    - values/redis-values.yaml
  
  #Additional Microservices listed here
```

### Manually Install Helmfile
- This [repository](https://github.com/helmfile/helmfile) contains the helmfile release, if you don't have the package managers and are using Linux you can do it manually with these commands
	- `cd /opt`
	- `mkdir helmfile`
	- `cd helmfile`
	- `wget https://github.com/helmfile/helmfile/releases/download/v0.161.0/helmfile_0.161.0_linux_amd64.tar.gz`
- `tar -xzvf helmfile_0.161.0_linux_amd64.tar.gz`
- `vim ~/.bashrc`
	- Add to the bottom: `export PATH=/opt/helmfile:$PATH`
- `reload ~/.bashrc`
-  The command now works
	- `helmfile --version`

### Deploy Helm Charts
- Verify local cluster is clear
	- `kubectl get pod`
- Install helmfile
	- `helmfile sync`

![Successful Helmfile Sync](/images/m10-8-successful-helmfile-sync.png)

### Uninstall Helm Chart
- `helmfile destroy`